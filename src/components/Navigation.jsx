import "./Navigation.css";

function Navigation(props) {
  const {user} = props // On récupère l'état de l'utilisateur par props drilling
  
  return (
    <ul className="Navigation">
      <li>
        <img src="https://via.placeholder.com/200x100" alt="Logo" />
      </li>
      <li>
        <h2>Awesome website!</h2>
      </li>
      {/* Si l'utilisateur est égal à null, on affiche "Please log in", sinon on affiche le nom de l'utilisateur*/}
      <li>{user == null ? "Please log in" : user.name }</li>
    </ul>
  );
}

export default Navigation;
